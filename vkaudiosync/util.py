 
 # fixes windows cmd issues
def safe_print(s, name):
    try:
        print s % name,
    except UnicodeEncodeError:
        name = "<Unprintable>"
        print s % name,
