import os

from vkaudiosync.vk import api
from vkaudiosync.download import download
from vkaudiosync.util import safe_print


class Sync(object):

    def __init__(self, local_dir):
        self._local_dir = local_dir

    def load_remote(self):
        self._load_albums()
        self._remote = self._format_remote(api.get())

    def _load_albums(self):
        """ {album_id: album_title} """
        self._albums = dict([(str(x["album_id"]), x["title"]) \
                             for x in api.get_albums()])

    def _format_remote(self, audio):
        """ {path: (aid, url)} """
        res = {}
        for track in audio:
            # TODO: warning about duplicates overwrite
            # TODO: form mp3 tags for each track here
            track_info = {
                "aid": track["aid"], 
                "url": track["url"],
            }
            res[self._path(track)] = track_info
        return res

    def _path(self, track):
        """ Where would we store the track locally. """
        #ext = os.path.splitext(track["url"])[-1]  # always mp3?
        ext = ".mp3"
        name = "%s - %s%s" % (track["artist"], track["title"], ext)
        name = _process_name(name)
        if "album" in track:
            album = self._albums[track["album"]]
            album = _process_name(album)
            name = os.path.join(album, name)
        name = os.path.join(self._local_dir, name)
        return name

    def load_local(self):
        res = []
        for dirpath, dirnames, filenames in os.walk(self._local_dir):
            res += map(lambda x: os.path.join(dirpath, x), filenames)
        self._local = res

    def sync(self):
        self._positive, self._negative = self._diff()
        remove(self._negative)
        self._cleanup()
        wins, fails = download(self._positive, self._remote)
        self.load_local()
        return wins, fails

    def _diff(self):
        positive = (x for x in self._remote if x not in self._local)
        negative = (x for x in self._local if x not in self._remote)
        return positive, negative

    def _cleanup(self):
        """ Remove empty dirs. """
        empty = []
        for dirpath, dirnames, filenames in os.walk(self._local_dir,
                                                    topdown=False):
            if dirpath == self._local_dir:
                continue

            dirnames = map(lambda x: os.path.join(dirpath, x), dirnames)
            dirnames = filter(lambda x: x not in empty, dirnames)
            if dirnames == filenames == []:
                empty.append(dirpath)
                os.rmdir(dirpath)

    def ensure_tags(self):
        # TODO:
        pass


def remove(gen):
    for path in gen:
        safe_print("Remove %s [y/N]?", path)
        answer = raw_input().strip()
        if answer in ('Y', 'y'):
            os.remove(path)


_table = {
     "\\": "&",
    "/": "&",
    ':': '',
    '*': '',
    '?': '',
    '"': '',
    '<': '',
    '>': '',
    '|': '',
}
def _process_name(name):
    """ Make the name safe for local filesystem. """
    for key, val in _table.iteritems():
        name = name.replace(key, val)
    return name
