import os
import sys
# make absolute imports work
main_dir = sys.path[0].rstrip("/")
abs_main_dir = os.path.join(os.getcwd(), main_dir)
sys.path.insert(0, os.path.dirname(abs_main_dir))


import vkaudiosync


vkaudiosync.run()
