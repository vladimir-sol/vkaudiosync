"""
Setup local http listener;
redirect default browser to vk auth page;
browser is supposed to be redirected back with an access token.
"""

import webbrowser
import urlparse
import time
import threading
import BaseHTTPServer

from vkaudiosync.vk.exceptions import CredentialsFail


# http://vk.com/developers.php?id=-1_37230422&s=1
_auth_url_raw = \
"https://oauth.vk.com/authorize?client_id=%(APP_ID)s&scope=%(SETTINGS)s&" \
"redirect_uri=%(REDIRECT_URI)s&display=%(DISPLAY)s&response_type=token"

_auth_url_params = {
    "APP_ID": 3476349,  # http://vk.com/editapp?id=3476349
    "SETTINGS": "audio",
    "REDIRECT_URI": "%s",
    "DISPLAY": "page",  # popup/mobile
}

_AUTH_URL = _auth_url_raw % _auth_url_params


_VK_REDIRECT = "http://oauth.vk.com/blank.html"  # for fallback auth
_MY_REDIRECT = "http://127.0.0.1:8083/fragment"


_html_base = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>VKaudiosync Authorization</title>
    </head>

    <body>
        <p>%s</p>
        %s
    </body>
</html>"""

_redirect_js = \
'<script>var x = document.URL; x = x.replace("://127.0.0.1:8083/fragment#",' \
'"://127.0.0.1:8083/query?"); window.location = x;</script>'

fragment_page = _html_base % ('Please wait...', _redirect_js)
query_page = _html_base % ('Keep calm and sync your audio.<br>' \
                          'This page can be closed now.', '')


class Handler(BaseHTTPServer.BaseHTTPRequestHandler):
    """
    VK passes back access token in the '#' (fragment) part of the url that is
    not visible from the http server.
    This http server responds to two types of requests: the one that has
    "/fragment" in it's path (initial vk redirect to '_MY_REDIRECT') is served
    with javascript page that transforms the fragment part into query string
    and redirects the browser again with "/query" path now. This is the second
    type of acceptable requests and the handler gets the access token from it.
    """
    # TODO: get rid of hardcoded strings (query, fragment)

    url = None

    def do_GET(self):
        assert self.client_address[0] == "127.0.0.1"
        self.send_response(200)
        self.end_headers()
        if self.path.startswith("/fragment"):
            self.wfile.write(fragment_page)
            self.wfile.close()
        elif self.path.startswith("/query"):
            Handler.url = "http://127.0.0.1" + self.path.replace("/query?", 
                                                                 "/fragment#")
            self.wfile.write(query_page)
            self.wfile.close()

    def log_message(self, format, *args):
        return


def _get_url_2():
    httpd = BaseHTTPServer.HTTPServer(('', 8083), Handler)
    Handler.url = None

    def job():
        while not Handler.url:
            httpd.handle_request()
    server = threading.Thread(target=job)
    server.start()
    time.sleep(1)

    webbrowser.open(_AUTH_URL % _MY_REDIRECT, new=2)
    time.sleep(2)

    server.join()

    return Handler.url


def _get_url():
    """ Fallback auth method: user has to manually copy the url """
    raw_input("VK authorization through browser is required [ENTER].")
    webbrowser.open(_AUTH_URL % _VK_REDIRECT, new=2)
    time.sleep(2)
    url = raw_input("Auth url: ").strip()
    return url


def get_credentials():
    """
    Return format:
    {
        access_token,
        expires_in,
        user_id,
    }
    """
    url = _get_url_2()
    creds = dict(urlparse.parse_qsl(urlparse.urlparse(url).fragment))

    if set(creds.keys()) != {"access_token", "expires_in", "user_id"}:
        raise CredentialsFail("Got %s instead of credentials" % \
                              str(creds.keys()))

    return creds

