import threading

from vkaudiosync.vk.auth.credentials import get_credentials
from vkaudiosync.vk.exceptions import InvalidToken


class _Auth(object):

    def __init__(self):
        self._expired_check_lock = threading.Lock()
        self._expire()

    def _reset(self):
        creds = get_credentials()
        self._token = creds["access_token"]
        self._uid = creds["user_id"]
        self._expires_in = creds["expires_in"]

    def _reset_if_expired(f):
        def wrapper(self, *args, **kwargs):
            with self._expired_check_lock:
                if self._expired:
                    self._reset()
                    self._expired = False
            return f(self, *args, **kwargs)
        return wrapper

    def _expire(self):
        self._expired = True

    @property
    @_reset_if_expired
    def token(self):
        return self._token

    @property
    @_reset_if_expired
    def uid(self):
        return self._uid
        

_AUTH = _Auth()


def authorize(f):
    """Responsible for passing a valid _Auth object as the first arg."""
    def wrapper(*args, **kwargs):
        try:
            return f(_AUTH, *args, **kwargs)
        except InvalidToken:
            _AUTH._expire()
            return f(_AUTH, *args, **kwargs)
    return wrapper

