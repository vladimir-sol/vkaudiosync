"""

Error msg:
{
    u'error': {
        u'error_code': 5,
        u'error_msg': u'User authorization failed: user revoke access
                        for this token.',
        u'request_params': [
            {
                u'key': u'oauth',
                u'value': u'1'
            },
            {
                u'key': u'method',
                u'value': u'audio.get'
            },
            {
                u'key': u'access_token',
                u'value': u''  # long token string
            }
        ]
    }
}

Song repr:
{
    u'aid': 195232764,
    u'album': u'34686918',  # optional
    u'artist': u'Seba',
    u'duration': 353,
    u'lyrics_id': u'70674549',
    u'owner_id': 4009399,
    u'title': u'Identity',
    u'url': u'http://cs6093.vk.me/u152469455/audios/fc8909dcb5d3.mp3'
}

"""

import urllib

import requests

from vkaudiosync.vk.auth import authorize
from vkaudiosync.vk.exceptions import InvalidToken
from vkaudiosync.exceptions import VkApiError


_METHOD_URL = \
"https://api.vk.com/method/%(METHOD_NAME)s?%(PARAMETERS)s&access_token=" \
"%(ACCESS_TOKEN)s"


def _check_response(response):
    try:
        response.raise_for_status()
        data = response.json()
    finally:
        response.close()

    if "response" in data:
        return data["response"]
    elif "error" in data:
        if data["error"]["error_code"] == 5:
            raise InvalidToken()
        else:
            raise VkApiError("Unknown error code: %s" % \
                data["error"]["error_code"])
    else:
        raise VkApiError("Unknown response: %s" % data.keys())


def _strip_useless(data_list, useless_keys):
    """ Reduce info about each object. """
    def strip(item):
        map(lambda x: item.pop(x, None), useless_keys)
        return item
    return map(strip, data_list)


def _validate_kwargs(*valid_kwargs):
    valid_kwargs = set(valid_kwargs)
    def decorator(f):
        def wrapper(*args, **kwargs):

            if args:
                raise VkApiError("Called with positional args: %s" % \
                    str(args))
            if not set(kwargs.keys()).issubset(valid_kwargs):
                raise VkApiError("Wrong kwargs: %s" % kwargs.keys())

            return f(*args, **kwargs)
        return wrapper
    return decorator


def _unescape(*keys):
    table = {
        "&amp;": "&",
    }
    def unescape_str(s):
        for key, val in table.iteritems():
            s = s.replace(key, val)
        return s

    def unescape_dict(d):
        for key in keys:
            if key not in d:
                continue
            d[key] = unescape_str(d[key])
    return unescape_dict

#
# API methods
#


@_validate_kwargs("album_id")
@authorize
def get(auth, **kwargs):

    params = {
        "METHOD_NAME": "audio.get",
        "PARAMETERS": urllib.urlencode(kwargs),
        "ACCESS_TOKEN": auth.token,
    }
    response = requests.get(_METHOD_URL % params)

    data = _check_response(response)

    data = _strip_useless(data, ["duration", "lyrics_id", "owner_id"])
    map(_unescape("artist", "title"), data)
    return data


@authorize
def get_albums(auth):
    params = {
        "METHOD_NAME": "audio.getAlbums",
        "PARAMETERS": '',
        "ACCESS_TOKEN": auth.token,
    }
    response = requests.get(_METHOD_URL % params)

    data = _check_response(response)

    # apparently data[0] is an int
    while data and isinstance(data[0], int):
        data.pop(0)

    data = _strip_useless(data, ["owner_id"])
    map(_unescape("title"), data)
    return data


def search():
    """
    Can be used to download tracks from a different server.
    """
    raise NotImplementedError()
