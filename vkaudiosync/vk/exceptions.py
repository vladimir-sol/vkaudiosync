from vkaudiosync.exceptions import VkApiError


class InvalidToken(VkApiError):
    pass


class CredentialsFail(VkApiError):
    pass
    