import os
import errno
import threading

import requests

from vkaudiosync.util import safe_print


def _ensure_path(dir_):
    try:
        os.makedirs(dir_)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
        else:
            assert os.path.isdir(dir_), str(dir_)


def download(gen, tracks):
    counters = {
        "wins": 0,
        "fails": 0,
    }
    lock = threading.Lock()

    def job(counters=counters, lock=lock):
        while True:
            with lock:
                try:
                    path = gen.next()
                except StopIteration:
                    break

            info = tracks[path]
            url = info["url"]

            safe_print("Downloading:\n" \
                       "  %s\n", path)
            response = requests.get(url)
            try:
                response.raise_for_status()
            except:
                safe_print("Failed:\n" \
                           "  %s\n", path)
                with lock:
                    counters["fails"] += 1
                continue

            _ensure_path(os.path.dirname(path))
            with open(path, 'wb') as f:
                f.write(response.content)
            safe_print("Finished:\n" \
                       "  %s\n", path)
            with lock:
                counters["wins"] += 1

    # 20 threads ~ 2.5 mb/s
    threads = [threading.Thread(target=job) for i in range(20)]
    map(lambda x: x.start(), threads)
    map(lambda x: x.join(), threads)

    return counters["wins"], counters["fails"]
