import sys
import os

from vkaudiosync.sync import Sync


def _music_dir_promt():

    def ask():
        return unicode(raw_input("Music dir: ")).strip()

    if len(sys.argv) == 2:
        path = unicode(sys.argv[1])
    else:
        path = ask()

    while not os.path.isdir(path):
        print "Error: directory %s doesn't exist." % path
        path = ask()
    return path


def run():
    path = _music_dir_promt()
    print "Music dir path set to %s." % path
    s = Sync(path)

    print "\nReading local collection..."
    s.load_local()

    print "\nLoading vk playlist..."
    s.load_remote()

    print "\nStarting sync..."
    wins, fails = s.sync()
    if fails:
        print "\nFailed to download %s tracks." % fails,
    print "\nSuccessfully downloaded %s tracks." % wins, "\nCompleted."
