"""
Windows only.
"""

import os
import shutil
import subprocess
import sys
import inspect
from distutils import archive_util


try:
    SOURCE_DIR_NAME = "vkaudiosync"
    EXE_NAME = "VKaudiosync"

    cacert_path = "C:\\Python27\\Lib\\site-packages\\requests\\cacert.pem"
    buildscript_path = inspect.getsourcefile(lambda: None)
    buildscript_dir = os.path.dirname(buildscript_path)
    project_dir = os.path.dirname(buildscript_dir)
    source_dir = os.path.join(project_dir, SOURCE_DIR_NAME)
    build_dir = os.path.join(buildscript_dir, "last_build")

    runner = """
import sys

# gotta distribute cacert.pem with the exe or disable ssl completely
# (requests.get(verify=False))
import os
os.environ["REQUESTS_CA_BUNDLE"] = "cacert.pem"

try:
    from %s import __main__\n
except:
    print "\\nCritical Error."
    print repr(sys.exc_info()[1])
    raw_input()
else:
    raw_input()
    """ % SOURCE_DIR_NAME
    runner_path = os.path.join(build_dir, "%s.py" % EXE_NAME)


    setup = """
from distutils.core import setup
import py2exe
setup(
    console=['%s'],
    zipfile=None,
    options={
        'py2exe': {
            'bundle_files': 1,
            'dll_excludes': ['w9xpopen.exe'],
        },
    },
)
    """ % os.path.basename(runner_path)
    setup_path = os.path.join(build_dir, "setup.py")

    # action

    try:
        shutil.rmtree(build_dir)
    except (WindowsError, OSError):
        pass
    shutil.copytree(source_dir, os.path.join(build_dir, SOURCE_DIR_NAME))

    with open(runner_path, 'w') as f:
        f.write(runner + "\n")
    with open(setup_path, 'w') as f:
        f.write(setup + "\n")

    os.chdir(build_dir)
    subprocess.call(["python", "setup.py", "py2exe"])

    # postprocess
    
    def clear_build_dir():
        for name in os.listdir(build_dir):
            if name == "dist":
                continue
            name = os.path.join(build_dir, name)
            if os.path.isfile(name):
                os.remove(name)
            elif os.path.isdir(name):
                shutil.rmtree(name)
    clear_build_dir()

    dist_dir = os.path.join(build_dir, "dist")
    named_dist_dir = os.path.join(build_dir, EXE_NAME)
    os.rename(dist_dir, named_dist_dir)
    shutil.copy(cacert_path, named_dist_dir)

    # WARNING: os.chdir
    os.chdir(os.path.dirname(named_dist_dir))
    archive_util.make_zipfile(EXE_NAME, os.path.basename(named_dist_dir))

except:
    exc = sys.exc_info()
    print repr(exc[1])
    raw_input()

else:
    raw_input()
    